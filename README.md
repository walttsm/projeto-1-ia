# Projeto 1 - Inteligência artificial prof. Ricardo

## Sobre o projeto

Este projeto faz parte das componentes avaliativas da disciplina.

O objetivo é criar um jogo simples para que um humano e dois agentes sejam capazes de resolver.

## Executando o projeto

Em um prompt de linha de comando, acesse a pasta do projeto e rode o arquivo jogo.py usando uma versão do python3 (O projeto foi desenvolvido usando a versão 3.9, mas o projeto deve funcionar em outras versões acima da 3.7.)

## Regras do jogo

### Objetivo

Você deve buscar o passageiro em seu local e levá-lo até o seu destino.

Ao deixar o passageiro no destino o jogo exibirá a mensagem 'Passageiro entregue!' e fechará.

### Sobre o jogo

O jogo consiste num mapa 2x2 com um taxi, um passageiro e um destino.

Exemplo de mapa:

```
[':', 'P']
['D', 'T']
```

No mapa temos que:

- 'T': Posição do taxi
- 'P': Posição do passageiro
- 'D': Posição de destino

Ao iniciar o jogo, um prompt irá perguntar qual o tipo de agente que irá jogar. Para escolher use as seguintes opções:

1. Agente Humano: Você irá digitar os comandos para o jogo
2. Agente BFS: Uma busca em largura nos estados do jogo será realizada para a resolução do jogo e os estados serão exibidos para o Humano
3. Agente DFS: Uma busca em profundidade nos estados do jogo será realizada para a resolução do jogo e os estados serão exibidos para o Humano

### Comandos possíveis

Caso opte por fornecer os comandos manualmente (opção de agente '1'), os comandos disponíveis são:

- Cima: O taxi anda uma casa para cima
- Baixo: O taxi anda uma casa para baixo
- Esquerda: O taxi anda uma casa para a esquerda
- Direita: O taxi anda uma casa para a direita

**Não é possível andar além da borda do mapa**

**Atenção para as letras maiúsculas nos comandos**
