#!/usr/bin/env python3

import time
from regras_jogo.regras_taxi import construir_jogo
from regras_jogo.personagens import Personagens
from agentes import construir_agente
from agentes.tipos import TiposAgentes

def ler_tempo(em_turnos=False):
    """ Se o jogo for em turnos, retorna a passada de 1 rodada.
    
    Se não for em turno, é continuo ou estratégico, retorna tempo
    preciso (ns) do relógio.
    """
    return 1 if em_turnos else time.time()


def iniciar_jogo():
    print("Selecione o tipo de agente que irá jogar o jogo: \n"\
        "1 - Humano\n"\
        "2 - Busca em largura (BFS)\n"\
        "3 - Busca em profundidade (DFS)\n"\
        "4 - Busca gulosa\n"\
        "5 - A*"
        )
    
    opcao = input()
    print("")

    if opcao == '1':
        agente = TiposAgentes.PREPOSTO_HUMANO
    elif opcao == '2':
        agente = TiposAgentes.AUTO_BFS
    elif opcao == '3':
        agente = TiposAgentes.AUTO_DFS
    elif opcao == '4':
        agente = TiposAgentes.AUTO_GULOSO
    elif opcao == '5':
        agente = TiposAgentes.AUTO_A_STAR
    else:
        raise Exception("Tipo de agente inexistente")

    # Inicializar e configurar jogo
    jogo = construir_jogo()
    personagem_jogador = jogo.registrarAgentePersonagem(Personagens.JOGADOR)
    agente_jogador = construir_agente(agente, Personagens.JOGADOR)
    
    tempo_de_jogo = 0
    while not jogo.isFim():
        
        # Mostrar mundo ao jogador
        ambiente_perceptivel = jogo.gerarCampoVisao(personagem_jogador)
        agente_jogador.adquirirPercepcao(ambiente_perceptivel)
        
        # Decidir jogada e apresentar ao jogo
        acao = agente_jogador.escolherProximaAcao()
        jogo.registrarProximaAcao(personagem_jogador, acao)

        # Atualizar jogo
        #tempo_corrente = ler_tempo()
        jogo.atualizarEstado(1)
        tempo_de_jogo += 1

    ambiente_perceptivel = jogo.gerarCampoVisao(personagem_jogador)
    agente_jogador.adquirirPercepcao(ambiente_perceptivel)
    jogo.terminarJogo(agente_jogador)


if __name__ == '__main__':
    iniciar_jogo()