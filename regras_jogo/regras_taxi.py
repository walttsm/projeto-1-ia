import logging, math
from abc import ABC, abstractmethod
from .regras_abstratas import AbstractRegrasJogo
from .personagens import Personagens
from .taxi import Taxi, Mapa, Passageiro, Destino
from percepcoes import PercepcoesJogador
from acoes import AcoesJogador, DirecaoMoverTaxi

class RegrasTaxi(AbstractRegrasJogo):
    """ Interface mínima para implementar um jogo interativo e modular. Não
    tente instanciar objetos dessa classe, ela deve ser herdada e seus métodos
    abstratos sobrecarregados.
    """

    def __init__(self) -> None:
        mapa_inicio = [[':','P'],['D','T']]
        self.mapa = mapa_inicio
        self.taxi = Taxi(1, 1, False)
        self.passageiro = Passageiro(0, 1)
        self.destino = Destino(1, 0)
        self.id_personagens = {Personagens.JOGADOR: 0}
        self.acoes_personagens = {0: None}

    def exec_acao(self, direcao) -> None:
        self.mapa[self.taxi.linha][self.taxi.coluna] = ':'
        if direcao == DirecaoMoverTaxi.CIMA:
            if self.taxi.linha != 0:
                self.taxi.linha -= 1
        elif direcao == DirecaoMoverTaxi.BAIXO:
            if self.taxi.linha != len(self.mapa) - 1:
                self.taxi.linha += 1
        elif direcao == DirecaoMoverTaxi.ESQUERDA:
            if self.taxi.coluna != 0:
                self.taxi.coluna -= 1
        elif direcao == DirecaoMoverTaxi.DIREITA:
            if self.taxi.coluna != len(self.mapa[0]) - 1:
                self.taxi.coluna += 1
        else:
            raise ValueError(f"Direção {direcao} inválida!")
        
        if self.taxi.embarcado == True:
            self.passageiro.linha = self.taxi.linha
            self.passageiro.coluna = self.taxi.coluna

        self.mapa[self.taxi.linha][self.taxi.coluna] = 'T'
        

    def registrarAgentePersonagem(self, personagem):
        """ Cria ou recupera id de um personagem agente.
        """
        return self.id_personagens[personagem]
    
    def isFim(self):
        """ Boolean indicando fim de jogo em True.
        """
        return self.passageiro.linha == self.destino.linha and self.passageiro.coluna == self.destino.coluna and self.taxi.embarcado == True

    def gerarCampoVisao(self, id_agente):
        """ Retorna um PercepcoesJogador para ser consumido por um agente
        específico. Objeto deve conter apenas descrição de elementos visíveis
        para este agente.

        PercepcoesJogador é um objeto imutável ou uma cópia do jogo, de forma que
        sua manipulação direta não tem nenhum efeito no mundo de jogo real.
        """
        return PercepcoesJogador((2, 2),(self.taxi.linha, self.taxi.coluna), self.taxi.embarcado, (self.passageiro.linha, self.passageiro.coluna), (self.destino.linha, self.destino.coluna), self.avaliacao())

    
    def registrarProximaAcao(self, id_agente, acao) -> None:
        """ Informa ao jogo qual a ação de um jogador especificamente.
        Neste momento, o jogo ainda não é transformado em seu próximo estado,
        isso é feito no método de atualização do mundo.
        """
        self.acoes_personagens[id_agente] = acao
    
    def atualizarEstado(self, diferencial_tempo):
        """ Apenas neste momento o jogo é atualizado para seu próximo estado
        de acordo com as ações de cada jogador registradas anteriormente.
        """
        acao_jogador = self.acoes_personagens[self.id_personagens[Personagens.JOGADOR]]
        if acao_jogador.tipo == AcoesJogador.MOVER_TAXI:
            direcao = acao_jogador.direcao
            self.exec_acao(direcao)
            if self.taxi.linha == self.passageiro.linha and self.taxi.coluna == self.passageiro.coluna:
                self.taxi.embarcado = True
                self.passageiro.linha = self.taxi.linha
                self.passageiro.coluna = self.taxi.coluna
        else:
            logging.warning(f"Recebi uma acao inválida, acao = {acao_jogador}.")
    
    def terminarJogo(self, agente_jogador):
        """ Faz procedimentos de fim de jogo, como mostrar placar final,
        gravar resultados, etc...
        """
        #print(agente_jogador.adquirirPercepcao(self.gerarCampoVisao(Personagens.JOGADOR)))
        print("\nPassageiro entregue!")
        return

    def avaliacao(self) -> int:
        distancia_taxi_obj: (int, int)
        if (self.taxi.embarcado == False):
            distancia_taxi_obj = (abs(self.taxi.linha - self.passageiro.linha) + abs(self.passageiro.linha - self.destino.linha),
                                  abs(self.taxi.coluna - self.passageiro.coluna) + abs(self.passageiro.coluna - self.destino.coluna))
        else:
            distancia_taxi_obj = (abs(self.taxi.linha - self.destino.linha),
                                  abs(self.taxi.coluna - self.destino.coluna))

        return distancia_taxi_obj[0] + distancia_taxi_obj[1]


def construir_jogo():
    """ Método factory para uma instância RegrasJogo arbitrária, de acordo com os
    parâmetros. Pode-se mudar à vontade a assinatura do método.
    """
    return RegrasTaxi()

   