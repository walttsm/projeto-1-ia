from typing import List
from dataclasses import dataclass

@dataclass
class Taxi():
    linha: int
    coluna: int
    embarcado: bool

    def __hash__(self) -> int:
        return hash(self.linha) + hash(self.coluna) + hash(self.embarcado)

    def __str__(self) -> str:
        return f'({self.linha},{self.coluna}, {self.embarcado})'

@dataclass
class Passageiro():
    linha:int
    coluna:int

    def __hash__(self) -> int:
        return hash(self.linha) + hash(self.coluna)

    def __str__(self) -> str:
        return f'({self.linha},{self.coluna})'

@dataclass
class Destino():
    linha: int
    coluna: int

    def __hash__(self) -> int:
        return hash(self.linha) + hash(self.coluna)

    def __str__(self) -> str:
        return f'({self.linha},{self.coluna})'

@dataclass
class Mapa:
    mapa: List[List[str]]
    taxi: Taxi
    passageiro: Passageiro

@dataclass
class Mover():
    taxi: Taxi
    direcao: str

    def __str__(self):
        return f'Mover({self.taxi}, {self.direcao})'