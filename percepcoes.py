from typing import Tuple, List
from dataclasses import dataclass

@dataclass
class PercepcoesJogador():
    '''Coloque aqui atributos que descrevam as percepções possíveis de
    mundo por parte do agente jogador
    
    Vide documentação sobre dataclasses em python.
    '''
    dimensoes: Tuple[int, int] = (2, 2)
    pos_taxi: Tuple[int, int] = (1, 1)
    embarcado: bool = False
    pos_passageiro: Tuple[int, int] = (0, 1)
    pos_destino: Tuple[int, int] = (1, 0)
    avaliacao: int = 0    
