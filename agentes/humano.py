from agentes.abstrato import AgenteAbstrato
from percepcoes import PercepcoesJogador
from acoes import DirecaoMoverTaxi, AcaoJogador

class AgentePrepostoESHumano(AgenteAbstrato):
    
    def adquirirPercepcao(self, percepcao_mundo: PercepcoesJogador):
        """ Inspeciona a disposicao dos elementos no objeto de visao e escreve
        na tela para o usuário saber o que seu agente está percebendo.
        """
        mapa=[]
        linhas, colunas = percepcao_mundo.dimensoes
        for _ in range(linhas):
            linha = []
            for _ in range(colunas):
                linha.append(':')
            mapa.append(linha)

        mapa[percepcao_mundo.pos_passageiro[0]][percepcao_mundo.pos_passageiro[1]] = 'P'
        mapa[percepcao_mundo.pos_destino[0]][percepcao_mundo.pos_destino[1]] = 'D'
        mapa[percepcao_mundo.pos_taxi[0]][percepcao_mundo.pos_taxi[1]] = 'T'

        print(mapa[0])
        print(mapa[1])
        print("\npassageiro embarcado = ", percepcao_mundo.embarcado)
        print("Avaliação = ", percepcao_mundo.avaliacao)
    
    def escolherProximaAcao(self):
        jogada = None
        while not jogada:
            jogada = input("\nEscreva sua jogada, as jogadas disponiveis são 'Cima', 'Baixo', 'Esquerda' e 'Direita':\n").strip()
            print("")

            if not self.is_jogada_valida(jogada):
                print("Jogada inválida, tente novamente")
                jogada = None

        direcoes = {
            'Cima': DirecaoMoverTaxi.CIMA,
             'Baixo': DirecaoMoverTaxi.BAIXO,
              'Esquerda': DirecaoMoverTaxi.ESQUERDA,
               'Direita': DirecaoMoverTaxi.DIREITA
        }

        return AcaoJogador.mover_taxi(direcoes[jogada])

    @classmethod
    def is_jogada_valida(cls, jogada):
        jogadas_validas = ['Cima', 'Baixo', 'Esquerda', 'Direita']
        return jogada in jogadas_validas
