from typing import List, Sequence, Optional, Tuple
from dataclasses import dataclass
import copy

@dataclass
class Taxi():
    linha: int
    coluna: int
    embarcado: bool

    def __hash__(self) -> int:
        return hash(self.linha) + hash(self.coluna) + hash(self.embarcado)

    def __str__(self) -> str:
        return f'B({self.linha},{self.coluna}, {self.embarcado})'

    def __eq__(self, value) -> bool:
        return self.linha == value.linha and self.coluna == value.coluna and self.embarcado == value.embarcado

@dataclass
class Passageiro():
    linha:int
    coluna:int

    def __hash__(self) -> int:
        return hash(self.linha) + hash(self.coluna)

    def __str__(self) -> str:
        return f'B({self.linha},{self.coluna})'

@dataclass
class Destino():
    linha: int
    coluna: int

    def __hash__(self) -> int:
        return hash(self.linha) + hash(self.coluna)

    def __str__(self) -> str:
        return f'B({self.linha},{self.coluna})'

@dataclass
class Mapa:
    mapa: List[List[str]]
    taxi: Taxi
    passageiro: Passageiro

@dataclass
class Mover():
    taxi: Taxi
    direcao: str

    def __str__(self):
        return f'Mover({self.taxi}, {self.direcao})'

@dataclass
class EstadoJogo:
    dim: int
    taxi: Taxi
    passageiro: Passageiro
    destino: Destino
    acao_ant: Optional[Mover]

class ProblemaTaxi():

    @staticmethod
    def estado_inicial() -> EstadoJogo:
        estado = EstadoJogo(
        dim = 2,
        taxi = Taxi(1, 1, False),
        passageiro = Passageiro(0, 1),
        destino = Destino(1,0),
        acao_ant = None
        )
        return estado

    @staticmethod
    def acoes(estado: EstadoJogo) -> Sequence[Mover]:
        acoes_possiveis = list()
        taxi = estado.taxi
        acao_ant = estado.acao_ant

        if taxi.linha != 0 and acao_ant != 'Baixo':
            acoes_possiveis.append(Mover(taxi, 'Cima'))
        if taxi.linha != estado.dim - 1 and acao_ant != 'Cima':
            acoes_possiveis.append(Mover(taxi, 'Baixo'))
        if taxi.coluna != 0 and acao_ant != 'Direita':
            acoes_possiveis.append(Mover(taxi, 'Esquerda'))
        if taxi.coluna != estado.dim - 1 and acao_ant != 'Esquerda':
            acoes_possiveis.append(Mover(taxi, 'Direita'))
        
        return acoes_possiveis
    
    @staticmethod
    def resultado(estado: EstadoJogo, acao: Mover) -> EstadoJogo:
        estado_resultante = EstadoJogo(
            estado.dim,
            copy.copy(estado.taxi),
            copy.copy(estado.passageiro),
            estado.destino,
            estado.acao_ant
            )
        taxi, direcao = acao.taxi, acao.direcao
        if direcao == 'Cima':
            if estado.taxi.linha != 0:
                estado_resultante.acao_ant = 'Cima'
                estado_resultante.taxi.linha -= 1
        elif direcao == 'Baixo':
            if estado.taxi.linha != estado.dim - 1:
                estado_resultante.acao_ant = 'Baixo'
                estado_resultante.taxi.linha += 1
        elif direcao == 'Esquerda':
            if estado.taxi.coluna != 0:
                estado_resultante.acao_ant = 'Esquerda'
                estado_resultante.taxi.coluna -= 1
        elif direcao == 'Direita':
            if estado.taxi.coluna != estado.dim - 1:
                estado_resultante.acao_ant = 'Direita'
                estado_resultante.taxi.coluna += 1
        else:
            raise ValueError(f"Direção {direcao} inválida!")


        if estado_resultante.taxi.embarcado == True:
            estado_resultante.passageiro.linha = estado_resultante.taxi.linha
            estado_resultante.passageiro.coluna = estado_resultante.taxi.coluna

        if estado_resultante.taxi.linha == estado_resultante.passageiro.linha and estado_resultante.taxi.coluna == estado_resultante.passageiro.coluna:
                estado_resultante.taxi.embarcado = True
                estado_resultante.passageiro.linha = estado_resultante.taxi.linha
                estado_resultante.passageiro.coluna = estado_resultante.taxi.coluna

        return estado_resultante
    
    @staticmethod
    def teste_objetivo(estado: EstadoJogo) -> bool:
        return estado.passageiro.linha == estado.destino.linha\
                                          and estado.passageiro.coluna == estado.destino.coluna\
                                          and estado.taxi.embarcado == True
    
    @staticmethod
    def custo(inicial: EstadoJogo, acao: Mover, resultante: EstadoJogo) -> int:
        """Custo em quantidade de jogadas"""
        return 1

    