from time import sleep

from percepcoes import PercepcoesJogador
from acoes import DirecaoMoverTaxi, AcaoJogador

from agentes.abstrato import AgenteAbstrato
from agentes.problemas.taxi import ProblemaTaxi
from .buscadores.busca import busca_a_star

class AgenteAStar(AgenteAbstrato):

    def __init__(self) -> None:
        super().__init__()
        self.problema: ProblemaTaxi = None
        self.solucao: list = []

    
    def adquirirPercepcao(self, percepcao_mundo: PercepcoesJogador):
        """ Inspeciona a disposicao dos elementos no objeto de visao e escreve
        na tela para o usuário saber o que seu agente está percebendo.
        """
        mapa=[]
        linhas, colunas = percepcao_mundo.dimensoes
        for _ in range(linhas):
            linha = []
            for _ in range(colunas):
                linha.append(':')
            mapa.append(linha)

        mapa[percepcao_mundo.pos_passageiro[0]][percepcao_mundo.pos_passageiro[1]] = 'P'
        mapa[percepcao_mundo.pos_destino[0]][percepcao_mundo.pos_destino[1]] = 'D'
        mapa[percepcao_mundo.pos_taxi[0]][percepcao_mundo.pos_taxi[1]] = 'T'

        print(mapa[0])
        print(mapa[1])
        print("\npassageiro embarcado = ", percepcao_mundo.embarcado)
        print("Avaliação = ", percepcao_mundo.avaliacao)
        
        if len(self.solucao) == 0:
            self.problema = ProblemaTaxi()
        
    def escolherProximaAcao(self):
        """ Escolhe a proxima acao do agente
        """
        sleep(2.)
        if not self.solucao:
            no_solucao = busca_a_star(self.problema)
            self.solucao = no_solucao.caminho_acoes()
            if not self.solucao:
                raise Exception("Agente não encontrou solução.")

        acao = self.solucao.pop(0)

        print(f"\nProxima Acao é {acao}\n")

        return AcaoJogador.mover_taxi(AgenteAStar.parse_jogada(acao))

    @staticmethod
    def parse_jogada(acao):
        direcoes = {
            'Cima': DirecaoMoverTaxi.CIMA,
             'Baixo': DirecaoMoverTaxi.BAIXO,
              'Esquerda': DirecaoMoverTaxi.ESQUERDA,
               'Direita': DirecaoMoverTaxi.DIREITA
        }
        jogada = acao.direcao
        return direcoes[jogada]
