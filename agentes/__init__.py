from .humano import AgentePrepostoESHumano
from .auto_bfs import AgenteBFS
from .auto_dfs import AgenteDFS
from .auto_guloso import AgenteGuloso
from .auto_a_star import AgenteAStar
from .tipos import TiposAgentes

def construir_agente(*args, **kwargs):
    """ Método factory para uma instância Agente arbitrária, de acordo com os
    parâmetros. Pode-se mudar à vontade a assinatura do método.
    """
    tipo_agente = args[0]
    if tipo_agente == TiposAgentes.PREPOSTO_HUMANO:
        return AgentePrepostoESHumano()
    elif tipo_agente == TiposAgentes.AUTO_BFS:
        return AgenteBFS()
    elif tipo_agente == TiposAgentes.AUTO_DFS:
        return AgenteDFS()
    elif tipo_agente == TiposAgentes.AUTO_GULOSO:
        return AgenteGuloso()
    elif tipo_agente == TiposAgentes.AUTO_A_STAR:
        return AgenteAStar()

    raise ValueError("Não foi escolhido nenhum agente")