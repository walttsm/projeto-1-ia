from typing import Any, Optional
from dataclasses import dataclass

import heapq

from agentes.problemas.taxi import Taxi
from acoes import DirecaoMoverTaxi, AcaoJogador
from agentes.problemas.taxi import Mover, ProblemaTaxi

class ProblemaSemSolucaoException(Exception):
    pass


@dataclass
class No():
    estado: ProblemaTaxi
    acao: Optional[Any] = None
    custo_solucao: int = 0
    pai: Optional['No'] = None

    def calcular_profundidade(self):
        raiz = not self.pai
        return 0 if raiz else self.pai.calcular_profundidade() + 1
    
    def caminho_acoes(self) -> list:
        raiz = not self.pai
        return [] if raiz else self.pai.caminho_acoes() + [self.acao]
    
    def __eq__(self, value):
        return self.estado.taxi == value.estado.taxi and self.custo_solucao == value.custo_solucao

    def __lt__(self, value):
        return self.custo_solucao < value.custo_solucao

    @classmethod
    def criar_no_filho(cls, problema, pai, acao, estrela):
        novo_estado = problema.resultado(pai.estado, acao)
        custo_solucao = problema.custo(pai.estado, acao, novo_estado)
        if (estrela):
            custo_solucao += pai.custo_solucao
        return cls(novo_estado, acao, custo_solucao, pai)
    
    def __repr__(self) -> str:
        return f'No({self.estado!r},{self.acao!r})'


def busca_em_largura(problema) -> No:
    """ Retorna uma solucao ou falha"""
    borda = [No(problema.estado_inicial())]
    while borda:

        folha = borda.pop(0)
            
        # print(f"Desenfileirando {folha}")
        if problema.teste_objetivo(folha.estado):
            return folha

        acoes = problema.acoes(folha.estado)

        # print(f'Não era objetivo. Ações adjacentes são {problema.acoes(folha.estado)}.')
        for acao in acoes:
            expandido = No.criar_no_filho(problema, folha, acao, False)
            #print ("\nExpandido =",expandido.estado)
            borda.append(expandido)
            # print(f'Enfileirado {expandido}')

    raise ProblemaSemSolucaoException()

def busca_em_profundidade(problema) -> No:
    """ Retorna uma solucao ou falha"""
    borda = [No(problema.estado_inicial())]
    while borda:

        folha = borda.pop()
        # print(f"Desenfileirando {folha}")
        if problema.teste_objetivo(folha.estado):
            return folha

        # print(f'Não era objetivo. Ações adjacentes são {problema.acoes(folha.estado)}.')
        for acao in problema.acoes(folha.estado):
            expandido = No.criar_no_filho(problema, folha, acao, False)
            borda.append(expandido)

            # print(f'Enfileirado {expandido}')

    raise ProblemaSemSolucaoException()


def busca_gulosa(problema) -> No:
    borda = [No(problema.estado_inicial())]
    while borda:
        folha = heapq.heappop(borda)
        #print("g =", folha.custo_solucao)

        if problema.teste_objetivo(folha.estado):
            return folha

        for acao in problema.acoes(folha.estado):
            expandido = No.criar_no_filho(problema, folha, acao, False)
            heapq.heappush(borda, expandido)

    raise ProblemaSemSolucaoException()

def busca_a_star(problema) -> No:
    borda = [No(problema.estado_inicial())]
    while borda:
        folha = heapq.heappop(borda)
        #print("h =", folha.custo_solucao)

        if problema.teste_objetivo(folha.estado):
            return folha

        for acao in problema.acoes(folha.estado):
            expandido = No.criar_no_filho(problema, folha, acao, True)
            heapq.heappush(borda, expandido)

    raise ProblemaSemSolucaoException()
        


busca_bfs = busca_em_largura
busca_dfs = busca_em_profundidade
