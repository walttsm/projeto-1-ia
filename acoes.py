from enum import Enum
from dataclasses import dataclass

class AcoesJogador(Enum):
    MOVER_TAXI = 'MoverTaxi'

class DirecaoMoverTaxi(Enum):
    DIREITA = 'Direita'
    ESQUERDA = 'Esquerda'
    CIMA = 'Cima'
    BAIXO = 'Baixo'

@dataclass
class AcaoJogador():
    tipo: str
    direcao: str = ''

    @classmethod
    def mover_taxi(cls, direcao: DirecaoMoverTaxi) -> 'AcaoJogador':
        return cls(AcoesJogador.MOVER_TAXI, direcao)